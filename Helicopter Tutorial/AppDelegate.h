//
//  AppDelegate.h
//  Helicopter Tutorial
//
//  Created by Hi Deff on 8/25/14.
//  Copyright (c) 2014 Mike Perry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
