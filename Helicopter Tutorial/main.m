//
//  main.m
//  Helicopter Tutorial
//
//  Created by Hi Deff on 8/25/14.
//  Copyright (c) 2014 Mike Perry. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
